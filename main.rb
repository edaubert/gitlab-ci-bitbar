require 'libnotify'
require 'json'
require_relative './lib/gitlab_pipeline_monitor.rb'


class LibnotifyNotifier
  def notify_failure project_name, project_branch, pipeline_url
    notification = Libnotify.new do |notify|
      notify.summary    = "CI failure on #{project_name} for branch #{project_branch}"
      notify.body       = "More information on #{pipeline_url}"
      notify.timeout    = 1.5         # 1.5 (s), 1000 (ms), "2", nil, false
      notify.urgency    = :normal   # :low, :normal, :critical
      notify.append     = false       # default true - append onto existing notification
      notify.transient  = false        # default false - keep the notifications around after display
      notify.icon_path  = "error"
    end
    notification.show!
  end
end

# check the set of variable we need to process everything
def check_configuration
  puts "You must define 'GITLAB_PIPELINE_MONITOR_CONFIGURATION' variable" if ENV['GITLAB_PIPELINE_MONITOR_CONFIGURATION'] == nil
  exit(-1) if ENV['GITLAB_PIPELINE_MONITOR_CONFIGURATION'] == nil
end

def get_endpoints
  # puts File.read(ENV['GITLAB_PIPELINE_MONITOR_CONFIGURATION'])
  configuration = JSON.parse(
    File.read(ENV['GITLAB_PIPELINE_MONITOR_CONFIGURATION'])
  )
  endpoint = configuration['endpoint'] || ''
  token = configuration['token'] || ''
  projects = configuration['projects'] || ''

  [
    ProjectListener.new(
      endpoint,
      token,
      projects
    )
  ]
end


# token = ENV["TOKEN"] || (print "Token: "; gets.strip)
# client = Slack::Client.new token: token
#
# Slack.configure do |config|
#   config.token = "YOUR_TOKEN"
# end
#
#
# Slack.auth_test
# Slack.chat_postMessage({})


check_configuration
project_listeners = get_endpoints
# process(project_listeners, nil)
process(project_listeners, LibnotifyNotifier.new)
