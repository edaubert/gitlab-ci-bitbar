require 'gitlab'

def retrieve_gitlab_client(endpoint, private_token)
  gitlab_client = Gitlab.client(endpoint: endpoint, private_token: private_token)
  gitlab_client
end

class ProjectListener
  attr_accessor :endpoint, :private_token, :projects

  def initialize(endpoint, private_token, projects)
    @endpoint = endpoint
    @private_token = private_token
    @projects = projects
  end
end

def get_status_icon(status)
  path = File.dirname(__FILE__)
  return "#{path}/../gitlab-green.png" if status == 'success'
  return "#{path}/../gitlab-red.png" if status == 'failed'
  return "#{path}/../gitlab-grey.png"
end

def get_projects(gitlab_client, project_descriptions)
  projects = []
  project_descriptions.each do |project_description|
    # puts project_description
    gitlab_projects = gitlab_client.project_search(project_description['name'], per_page: 100)
    # if project_description['path'] != nil && project_description['path'] != ''
    #   gitlab_projects.select! do |project|
    #     project.path_with_namespace.include?project_description['path']
    #   end
    # end
    # puts gitlab_projects.inspect
    gitlab_projects.each do |potential_project|
      if is_corresponding(project_description, potential_project)
          projects << { 'project' => potential_project, 'branch' => project_description['branch'], 'notify' => project_description['notify'] }
      end
    end
  end
  # puts projects
  projects
end

def is_corresponding(project_description, project)
  return true if project_description['path'] == nil || project_description['path'] == ''
  return true if project.path_with_namespace.include?project_description['path']
end

def get_global_status(_gitlab_client, project_statuses)
  global_status = 'success'
  project_statuses.each do |project_status|
    # puts project_status
    if project_status['status'] != 'failed' && global_status == 'success'
      global_status = 'success'
    else
      global_status = 'failed'
    end
  end
  # puts global_status
  global_status
end

def get_project_statuses(gitlab_client, projects)
  projects_statuses = []
  projects.each do |project|
    # puts project['project'].name
    # puts project['branch']
    pipelines = gitlab_client.pipelines(
      project['project'].id,
      order_by: 'id',
      sort: 'desc',
      per_page: 1,
      ref: project['branch']
    )
    # puts pipelines
    unless pipelines.empty?
      # puts pipelines[0].inspect
      projects_statuses << {
        'project' => project,
        'status' => pipelines[0].status,
        'pipeline_ref' => pipelines[0].ref,
        'pipeline_id' => pipelines[0].id,
        'notify' => project['notify']
      }
    end
  end
  # puts projects_statuses
  projects_statuses
end

def display_global_status(global_status)
  puts " | iconName=#{get_status_icon(global_status)} dropdown=false"
  puts '---'
end

def notify_failure(project_status, notifier)
    # puts project_status['project']['project'].name
    # puts project_status['pipeline_ref']
    # puts "#{project_status['project']['project'].web_url}/pipelines/#{project_status['pipeline_id']}"
    # puts project_status['project']['notify']
    if project_status['project']['notify'] == true
      notifier.notify_failure(
        project_status['project']['project'].path_with_namespace,
        project_status['pipeline_ref'],
        "#{project_status['project']['project'].web_url}/pipelines/#{project_status['pipeline_id']}"
      )
    end
end

def display_project_status(project_status)
  # puts project_status["project"]["project"].inspect
  puts "#{project_status['project']['project'].path_with_namespace} #{project_status['pipeline_ref']}| iconName=#{get_status_icon(project_status['status'])} href='#{project_status['project']['project'].web_url}/pipelines/#{project_status['pipeline_id']}'"
end

def process_project_statuses(project_statuses, notifier)
  puts '<i><b>Click to see the pipeline</b></i>'
  project_statuses.each do |project_status|
    display_project_status project_status
    notify_failure(project_status, notifier)  if project_status['status'] == 'failed'
  end
end

def process(project_listeners, notifier)
  project_listeners.each do |project_listener|
    # puts "project_listener"
    gitlab_client = retrieve_gitlab_client(project_listener.endpoint, project_listener.private_token)
    projects = get_projects(gitlab_client, project_listener.projects)
    project_statuses = get_project_statuses(gitlab_client, projects)
    global_status = get_global_status(gitlab_client, project_statuses)

    display_global_status(global_status)
    process_project_statuses(project_statuses, notifier)
  end
end
