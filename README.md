# Gitlab CI statuses

This project display the status of the last pipelines on Gitlab CI for specific projects and specific branches.

## How it works

This script can be used with [bitbar](https://getbitbar.com/) or with [Argos for Cinnamon](https://github.com/Odyseus/CinnamonTools/tree/master/applets/0dyseus%40ArgosForCinnamon) (on Linux with Cinnamon as Desktop Environment) to display the statuses of the pipelines as a icon in the panel.

When  we click on this icon, it displays a menu item with each status for each couple project/branch you registered.

## Dependencies
This script is written in ruby so you need to have it on your computer (Please refer to https://www.ruby-lang.org/en/ to install it) and you also need to install the dependencies defined on the ./GemFile.

If you don't know how to do it, Google is your friend ;-)

But if you don't want to call Google, follow this steps:

```
gem install bundler
bundler install
```

it will install `bundler` which is like a package manager (but do a lot more) for ruby. Then bundler will install everything you need.

## How to use it

You need to define a configuration file following the one defined as ./configuration

Then you can update the gitlab-pipeline-monitor.bash to point to this new configuration file.
